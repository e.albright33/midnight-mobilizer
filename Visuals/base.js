import {Employee} from "../Employees/Employee.js";
import {Desk} from "../Desks/Desk.js";
import {MongoClient} from "mongodb";
var day_first_avail = [];
var day_second_avail = [];
var day_all_avail = [];

async function main(start, end){
    var all_employees = [];
    var all_desks = [];

    const uri = "mongodb+srv://dues:BTa2YTsGrrW5Ey3L@cluster0.xxmr19b.mongodb.net/?retryWrites=true&w=majority";;
    const client = new MongoClient(uri);
    await client.connect();
    const db = client.db("midnight-mobilizer");
    const employees = db.collection("employees");
    const desks = db.collection("desks");

    const query = {"preferred_name":{"$ne": null}};
    const cursor = employees.find(query);

    const desk_query = {"name":{"$ne":null}};
    const desk_cursor = desks.find(desk_query);

    for await (const desk_doc of desk_cursor){
        var newDesk = new Desk(desk_doc.name, desk_doc.location, desk_doc.weekend_only, desk_doc.first_half, desk_doc.second_half, desk_doc.priority);
        all_desks.push(newDesk);
    }

    for await (const doc of cursor){
        var new_Employee = new Employee(doc.first_name, doc.last_name, doc.preferred_name, doc.role);
        new_Employee.setPhoneNumber(doc.phone_number);
        new_Employee.setEmail(doc.email);
        new_Employee.setAvailabilityFirst(doc.availability_first);
        new_Employee.setAvailabilitySecond(doc.availability_second);
        new_Employee.setMinHours(doc.min_hours);
        new_Employee.setMaxHours(doc.max_hours);
        if(!doc.desk){
            new_Employee.setDesk(null);
        }else{
            for(var desk_i = 0; desk_i < all_desks.length; desk_i++){
                if(all_desks[i].getName() == doc.desk){
                    new_Employee.setDesk(all_desks[i].getName() == doc.desk);
                }
            }
        }
        for(var dt = 0; dt < doc.asked_off.length; dt++){
            var m = new Map();
            for(var day = start; day <= end; day += day_in_mili){
                if(day == new Date(doc.asked_off[d]).getTime()){
                    m.set(new Date(doc.asked_off[dt]).toDateString(), true);
                }else{
                    m.set(new Date(doc.asked_off[dt]).toDateString(), false);
                }
            }
            new_Employee.setAskedOff(m);
        }
        all_employees.push(new_Employee);
    }

    //months start at 0 -> january
    var day_in_mili = 8.64e+7
    var chosen;
    
    for(var d = new Date(start).getTime(); d <= new Date(end).getTime(); d += day_in_mili){
        var date = new Date(d);
        for(var i = 0; i < all_desks.length; i++){
                for(var p = 0; p < all_employees.length; p++){
                    var person = all_employees[p];
                    var asked_off = person.getAskedOff();
                if((person.getAvailabilityFirst()[0] && person.getAvailabilitySecond()[0]) && !asked_off.get(date.toDateString())){
                    day_all_avail.push(person);
                    day_first_avail.push(person);
                    day_second_avail.push(person);
                }else if(person.getAvailabilityFirst()[0] && !asked_off.get(date.toDateString())){
                    day_first_avail.push(person);
                } else if(person.getAvailabilitySecond()[0] && !asked_off.get(date.toDateString())){
                    day_second_avail.push(person);
                }
            }
            chosen = scheduler(all_desks[i], day_first_avail, day_second_avail, day_all_avail, date);
            all_desks[i].setPerson(chosen);
            console.log(chosen.getPreferredName() + " " + all_desks[i].getName() + "\n");
        }
    }

}


function scheduler(desk, first_avail, second_avail, day_all_avail, date){
    var chosen = null;
    if(desk.getFirstHalf() && desk.getSecondHalf()){
        chosen = weightedRandom(day_all_avail);
        removeElement(day_all_avail, chosen);
        removeElement(day_first_avail, chosen);
        removeElement(day_second_avail, chosen);
    }else if(desk.getFirstHalf()){
        chosen = weightedRandom(first_avail);
        removeElement(first_avail, chosen);
    }else{
        chosen = weightedRandom(second_avail);
        removeElement(second_avail, chosen);
    }

    return chosen;
}

// credit Weighted Random Algorithm
function weightedRandom(avail){
    var weight = 0;
    for(var i = 0; i < avail.length; i++){
        weight += avail[i].getMinHours();
    }

    const random = Math.ceil(Math.random() * avail.length)

    let cursor = 0
    for (let i = 0; i < avail.length; i++) {
        cursor += avail[i].getMinHours();
        if (cursor >= random) {
            return avail[i];
        }
    }
}


//credit: sentry https://sentry.io/answers/remove-specific-item-from-array/
function removeElement(avail, to_remove){

    const index = avail.indexOf(to_remove);

    const x = avail.splice(index, 1);

}

main(new Date(2024, 1, 1).getTime(), new Date(2024, 1, 7).getTime());