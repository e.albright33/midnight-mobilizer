export class DeskModel {
    name; // string
    location; // string
    weekend_only; // bool
    first_half; // bool
    second_half; // bool
    priority; // int
    person;
}