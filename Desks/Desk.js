import {DeskModel} from "./DeskModel.js";

export class Desk {
    newDesk = new DeskModel();
    constructor(name, location, weekend_only, first_half, second_half, priority){
        this.setName(name);
        this.setLocation(location);
        this.setWeekendOnly(weekend_only);
        this.setFirstHalf(first_half);
        this.setSecondHalf(second_half);
        this.setPriority(priority);
    }

    getName() {
        return this.newDesk.name;
    }

    setName(n) {
        this.newDesk.name = n;
    }
    
    getLocation() {
        return this.newDesk.location;
    }

    setLocation(l) {
        this.newDesk.location = l;
    }

    getWeekendOnly() {
        return this.newDesk.weekend_only;
    }

    setWeekendOnly(w) {
        this.newDesk.weekend_only = w;
    }
    
    getFirstHalf(){
        return this.newDesk.first_half;
    }
    
    setFirstHalf(first){
        this.newDesk.first_half = first;
    }
    
    getSecondHalf(){
        return this.newDesk.second_half;
    }

    setSecondHalf(second){
        this.newDesk.second_half = second;
    }

    getPriority(){
        return this.newDesk.priority;
    }

    setPriority(priority){
        this.newDesk.priority = priority;
    }

    getPerson(){
       return this.newDesk.person;
    }

    setPerson(person){
        this.newDesk.person = person;
    }
}