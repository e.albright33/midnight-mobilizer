export class EmployeeModel {
    role; // String
    phone_number; // String
    email; // String
    first_name; // String
    last_name; // String
    preferred_name; // String
    availability_first; // first half availability Array size 7
    availability_second; // second half availability Array size 7
    min_hours; // int
    max_hours; // int
    desk; // type desk
    asked_off; // map

    
}