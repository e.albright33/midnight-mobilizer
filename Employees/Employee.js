import {EmployeeModel} from "./EmployeeModel.js";

export class Employee {
    model = new EmployeeModel();

    constructor(first_name, last_name, preferred_name, role) {
        this.setFirstName(first_name);
        this.setLastName(last_name);
        this.setPreferredName(preferred_name);
        this.setRole(role);
    }

    getRole() {
        return this.model.role;
    }

    setRole(r) {
        this.model.role = r;
    }
    
    getPhoneNumber() {
        return this.model.phone_number;
    }

    setPhoneNumber(p) {
        this.model.phone_number = p;
    }

    getEmail() {
        return this.model.email;
    }

    setEmail(e) {
        this.model.email = e;
    }
    
    getFirstName() {
        return this.model.first_name;
    }

    setFirstName(fn) {
        this.model.first_name = fn;
    }

    getLastName() {
        return this.model.last_name;
    }

    setLastName(ln) {
        this.model.last_name = ln;
    }

    getPreferredName() {
        return this.model.preferred_name;
    }

    setPreferredName(pn) {
        this.model.preferred_name = pn;
    } 

    getAvailabilityFirst(){
        return this.model.availability_first;
    }

    setAvailabilityFirst(availability){
        this.model.availability_first = availability;
    }


    getAvailabilitySecond(){
        return this.model.availability_second;
    }

    setAvailabilitySecond(availability){
        this.model.availability_second = availability;
    }

    getMinHours(){
        return this.model.min_hours;
    }

    setMinHours(hours){
        this.model.min_hours = hours;
    }
    
    getMaxHours(){
        return this.model.max_hours;
    }

    setMaxHours(hours){
        this.model.max_hours = hours;
    }

    getDesk(){
        return this.model.desk;
    }

    setDesk(desk){
        this.model.desk = desk;
    }
    
    getAskedOff(){
        return this.model.asked_off;
    }

    setAskedOff(off){
        this.model.asked_off = off;
    }
    
    
}